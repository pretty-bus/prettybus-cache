#!/usr/bin/env bash

sudo apt-get install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://ftp.heanet.ie/mirrors/mariadb/repo/10.1/ubuntu trusty main'

add-apt-repository ppa:chris-lea/redis-server
apt-get update
apt-get install -y mariadb-server build-essential tcl redis-server redis-tools python-pip

pip install virtualenv
pip install virtualenvwrapper


# set up virtualenvwrapper
mkdir /home/vagrant/.venvs
echo "WORKON_HOME=/home/vagrant/.venvs" >> /home/vagrant/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> /home/vagrant/.bashrc
source /home/vagrant/.bashrc


# set up a virtual env
mkvirtualenv simpleToken
cd /vagrant
pip install -r requirements.txt
