import sys
import datetime
from git import Repo
from termcolor import cprint
from fabric.utils import abort
from fabric.api import local, task, settings
from fabric.context_managers import lcd
from fabric.contrib.console import confirm
from fabric.tasks import execute


text_colours = {
  'info': 'cyan',
  'warn': 'yellow',
  'error': 'red',
}

repo = Repo('/home/daniel/projects/prettybus-cache')
if repo.is_dirty():
    cprint(
        'Repo is dirty. Commit or stash your changes.',
        text_colours['error']
    )
    sys.exit()

"""
    todo: Still need to implement rollback
"""


@task
def tag():
    sha = repo.head.commit.hexsha
    shortSha = repo.git.rev_parse(sha, short=6)
    timestamp = datetime.datetime.now().strftime('%Y%m%d%H')
    tagName = "{}_{}".format(shortSha, timestamp)
    if tagName not in repo.tags:
        repo.create_tag(tagName)
        cprint("Created new tag: {}".format(tagName), text_colours['info'])
    else:
        cprint(
            "Tag \"{}\" already exists".format(tagName),
            text_colours['warn']
        )
    cprint("Pushing tag up to origin", 'cyan')
    tag = repo.tags[tagName]
    origin = repo.remotes.origin
    origin.push(tag)


@task
def deploy():
    # generate changelog
    tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
    current_tag = tags[-1].name
    previous_tag = tags[-2].name

    cprint(
        "Generating diff {}...{}".format(current_tag, previous_tag),
        text_colours['info']
    )
    changelog = repo.git.log(
      '--oneline',
      '--decorate',
      '{}...{}'.format(current_tag, previous_tag)
    )
    cprint("Changelog: {}".format(changelog), text_colours['info'])
    if not confirm("Deploy these changes ?", default=True):
        abort("aborting at users request")

    # run the unittests
    with settings(warn_only=True):
        result = local(
            "/Users/Daniel/.virtualenvs/prettybus-cache/bin/nosetests"
        )
        if result.return_code != 0:
            if not confirm("Tests are failing. Continue ?", default=False):
                abort("aborting.. Tests are failing")

    cprint("Deploying changes", text_colours['info'])
    repo.remotes.heroku.push()
    repo.remotes.origin.push()
    cprint("Changes Deployed!", text_colours['info'])


@task
def deploy_now():
    cprint("Deploying now!")
    execute(tag)
    execute(deploy)
