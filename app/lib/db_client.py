import logging
import requests
from requests.exceptions import Timeout

from app.lib import Borg
from app.lib.Exceptions import *
from app.DataAccess.cache import Cache
from app.DataAccess.config import get_current_config


class DBClient(Borg):
    def __init__(self):
        Borg.__init__(self)
        self.cache = Cache()
        self.config = get_current_config()

    def get_all_stops(self):
        # check the cache
        stops = self.cache.get_all_stops()
        if stops is None:
            # not in the cache do an request
            response = self._do_call(self.config.DB_URLS['all_stops'])
            # store it in the cache
            self.cache.store_all_stops(response)
            return response
        else:
            return stops

    def get_rtpi(self, stop_id):
        # check the cache
        rtpi = self.cache.get_rtpi(stop_id)
        if rtpi is None:
            # not in the cache do an request
            response = self._do_call(
                (self.config.DB_URLS['rtpi']).format(stop_id)
            )

            # store it in the cache
            self.cache.store_rtpi(stop_id, response)
            return response
        else:
            return rtpi

    def get_stop_info(self, stop_id):
        # check the cache
        stop = self.cache.get_stop_info(stop_id)
        if stop is None:
            # not in the cache do an request
            response = self._do_call(
                (self.config.DB_URLS['stop']).format(stop_id)
            )

            # store it in the cache
            self.cache.store_stop_info(stop_id, response)
            return response
        else:
            return stop

    def _do_call(self, url):
        try:
            response = requests.get(url)
            parsed_json = response.json()
            if response.status_code == 200:
                parsed_json = response.json()
                if parsed_json['errorcode'] == "1":
                    """
                        response 200 but error could still have happend
                        TODO: check for other api errors
                    """
                    self.logger.warn("Stop not found: {}".format(url))
                    raise StopNotFoundError
                return response.text
            elif response.status_code == 404:
                self.logger.warn("Stop not found: {}".format(url))
                raise StopNotFoundError
            elif response.status_code == 502:
                self.logger.warn("502 Bad Gateway,: {}". format(url))
                raise DownStreamError
            else:
                raise UnexpectedError
        except Timeout:
            self.logger.warn("request Timed out: {}".format(url))
            raise DownStreamError
