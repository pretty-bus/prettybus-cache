import json
import datetime
import iso8601

from zeep import Client
from zeep.helpers import serialize_object

from app.DataAccess.cache import Cache
from app.DataAccess.config import get_current_config
from app.lib.Exceptions import DownStreamError


class SoapClient():
    def __init__(self):
        self.cache = Cache(prefix="v2")
        self.config = get_current_config()
        self.client = Client(self.config.DB_URLS['soap'])

    def get_all_stops(self):
        stops = json.loads(self.cache.get_all_stops(default='[]'))

        if not stops:
            result = (self.client.service.GetDestinations(filter="")
                      ['Destination'])
            stops = [
                    {
                        k: v
                        for k, v in serialize_object(item).items()
                        if v is not None
                    }
                    for item in result
                ]
            self.cache.store_all_stops(json.dumps(stops))
        return stops

    def _timestamp_to_diff(self, timestamp):
        diff = 0
        try:
            timestamp = iso8601.parse_date(timestamp)
        finally:
            # make sure that we're using the same timezone
            now = datetime.datetime.now(tz=timestamp.tzinfo)
            diff = (timestamp - now).total_seconds() / 60
            if diff < 1:
                return "due"
            return "{:.0f}".format(diff)

    def get_rtpi(self, stop_id):
        rtpi = json.loads(self.cache.get_rtpi(stop_id, default='[]'))

        if not rtpi:
            results = []
            try:
                results = self.client.service.GetRealTimeStopData(
                                stopId=stop_id,
                                forceRefresh='false'
                            )['_value_1'][1][0]
            except IndexError:
                """
                    This is very generic, it could mean that no data was found
                    or the stop doesnt exist
                    TODO: see if there is any difference between these.
                """
                raise DownStreamError

            response_mapping = {
                    'MonitoredVehicleJourney_PublishedLineName': 'route',
                    'MonitoredCall_ExpectedArrivalTime': 'due',
                    'MonitoredVehicleJourney_DestinationName': 'destination'
            }
            """
                result contains an array of arrays
                each sub array contains realtime information for a bus
                convert those sub arrays into dicts
                filtering out useless information
            """
            for result in results:
                tmp = {}
                for item in result:
                    if item.tag in response_mapping:
                        if item.tag == 'MonitoredCall_ExpectedArrivalTime':
                            tmp[response_mapping[item.tag]] = \
                                self._timestamp_to_diff(item.text)
                        else:
                            tmp[response_mapping[item.tag]] = item.text
                rtpi.append(tmp)
            self.cache.store_rtpi(stop_id, json.dumps(rtpi))
        return rtpi

    def get_stop_info(self, stop_id):
        stop_data = json.loads(self.cache.get_stop_info(stop_id, default='{}'))

        if not stop_data:
            results = (self.client.service.GetDestinations(filter=stop_id)
                       ['Destination'])
            for result in results:
                if result['StopNumber'] == long(stop_id):
                    stop_data = {
                        k: v
                        for k, v in serialize_object(result).items()
                        if v is not None
                    }
                    break

            self.cache.store_stop_info(stop_id, json.dumps(stop_data))
        return stop_data
