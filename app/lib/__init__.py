import logging
from app.DataAccess.config import get_current_config


class Borg:
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state
        self.config = get_current_config()
        self.logger = logging.getLogger(self.config.LOGGER_NAME)
