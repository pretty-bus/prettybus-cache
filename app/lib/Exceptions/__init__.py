from flask_restful import HTTPException


class JSONParseError(HTTPException):
    pass


class UnexpectedError(HTTPException):
    pass


class StopNotFoundError(HTTPException):
    pass


class DownStreamError(HTTPException):
    pass
