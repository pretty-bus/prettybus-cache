from flask import Flask
from flask_restful import Api
from flask_cors import CORS, cross_origin

from bootstrap import load_routes
from app.DataAccess.config import get_current_config
from app.lib.db_client import DBClient

application = Flask(__name__)

config = get_current_config()
application.config.from_object(config)

CORS(application)
api = Api(application, errors=config.ERRORS)
api = load_routes(api)


DBClient()

if not config.TESTING:
    import logging
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARNING)
    application.logger.addHandler(stream_handler)


@application.errorhandler(Exception)
def error_handler(e):
    application.logger.warning("Unhandled Exception: {}\n{}"
                               .format(type(e), str(e)))
    return '{"message": "Internal Error", "status": 502}', 502
