from app.config import BaseConfig


class TestConfig(BaseConfig):
    # TODO: fix this its not ideal to copy stuffs from BaseConfig
    REDIS_CONNECTION = {
        'host': 'localhost',
        'port': '6379',
        'db': 1
    }

    TESTING = True
