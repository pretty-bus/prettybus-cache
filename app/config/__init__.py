
class BaseConfig:
    TESTING = False
    DEBUG = True
    LOGGER_NAME = 'app'
    LOG_FILE = 'app.log'

    REDIS_CONNECTION = {
        'host': 'localhost',
        'port': '6379',
        'db': 0
    }

    ERRORS = {
        'JSONParseError': {
            'message': 'Whoa thats not JSON',
            'status': 504
        },
        'UnexpectedError': {
            'message': ('Something bad happened;'
                        ' Well thats not good, now is it'),
            'status': 505
        },
        'StopNotFoundError': {
            'message': 'Stop not found; are we in the same city?',
            'status': 404
        },
        'DownStreamError': {
            'message': 'Something bad happened: DownStreamError',
            'status:': 506
        }
    }

    DB_URLS = {
        'all_stops': ('https://data.dublinked.ie/cgi-bin/rtpi/'
                      'busstopinformation?operator=bac'),
        'rtpi': ('https://data.dublinked.ie/cgi-bin/rtpi/'
                 'realtimebusinformation?stopid={}'),
        'stop': ('https://data.dublinked.ie/cgi-bin/rtpi/'
                 'busstopinformation?stopid={}'),
        'soap': 'http://rtpi.dublinbus.ie/DublinBusRTPIService.asmx?WSDL'
    }

    def has(self, config_name):
        """
        check if the config obj has the
        named config and it is not None
        """
        return (hasattr(self, config_name)
                and getattr(self, config_name) is not None)
