import os
from app.config import BaseConfig


class HerokuConfig(BaseConfig):
    DEBUG = False
    REDIS_CONNECTION = None
    REDIS_URL = os.getenv('REDIS_URL')
