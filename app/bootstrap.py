from .route.v1.stop import StopViewV1 as StopV1
from .route.v1.rtpi import RTPIViewV1 as RTPIV1
from .route.v2.stop import StopViewV2 as StopV2
from .route.v2.rtpi import RTPIViewV2 as RTPIV2
from .route.Base.status import StatusView


def load_routes(api):

    views = [StopV1, RTPIV1, StopV2, RTPIV2, StatusView]
    for view in views:
        api.add_resource(view, *view.available_routes)
    return api
