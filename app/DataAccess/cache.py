import redis
import urlparse
from app.DataAccess.config import get_current_config


class Cache:
    def __init__(self, prefix=None):
        config = get_current_config()
        self.redis = None
        self.prefix = prefix

        if config.has('REDIS_CONNECTION'):
            self.redis = redis.StrictRedis(**config.REDIS_CONNECTION)
        elif config.has('REDIS_URL'):
            url = urlparse.urlparse(config.REDIS_URL)
            self.redis = redis.StrictRedis(
                host=url.hostname,
                password=url.password,
                port=url.port
            )

    def connected(self):
        try:
            return self.redis.ping() == 'PONG'
        except redis.exceptions.ConnectionError:
            return False

    def get_all_stops(self, *args, **kwargs):
        return self._base_get('all_stops', *args, **kwargs)

    def store_all_stops(self, stop_data):
        self._base_set('all_stops', stop_data, ttl=3600)

    def get_stop_info(self, stop_id, *args, **kwargs):
        return self._base_get('stop_{}'.format(stop_id), *args, **kwargs)

    def store_stop_info(self, stop_id, stop_data):
        self._base_set('stop_{}'.format(stop_id), stop_data, ttl=3600)

    def get_rtpi(self, stop_id, *args, **kwargs):
        return self._base_get('rtpi_{}'.format(stop_id), *args, **kwargs)

    def store_rtpi(self, stop_id, stop_data):
        self._base_set('rtpi_{}'.format(stop_id), stop_data, ttl=5)

    def _base_get(self, key, default=None, *args, **kwargs):
        if self.prefix is not None:
            key = '{}_{}'.format(self.prefix, key)
        data = self.redis.get(key)
        if data is None:
            return default
        return data

    def _base_set(self, key, data, ttl=30):
        if self.prefix is not None:
            key = '{}_{}'.format(self.prefix, key)
        self.redis.set(key, data, ex=ttl)
