class DataAccess:
    def __init__(self, connectionInfo):
        pass

    def get(self, obj_id):
        raise NotImplementedError

    def save(self, obj):
        raise NotImplementedError
