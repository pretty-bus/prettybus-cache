import os
import sys
from app.config import BaseConfig
from app.config.devConfig import DevConfig
from app.config.testConfig import TestConfig


def get_current_config():
    config = BaseConfig()
    if os.getenv('ENV') == 'DEV':
        config = DevConfig()
    elif os.getenv('ENV') == 'TEST':
        config = TestConfig()
    elif os.getenv('ENV') == 'HEROKU':
        try:
            from app.config.herokuConfig import HerokuConfig
            config = HerokuConfig()
        except ImportError:
            print >> sys.stderr, "Heroku ENV but no Heroku config file WTF!!"

    config.ENV = os.getenv('ENV', 'Unknown')
    return config
