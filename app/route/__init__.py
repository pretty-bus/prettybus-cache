from flask import make_response


class ResourceMixin:
    available_routes = []
    decorators = []
    api_version = ''


def version(version):
    """This decorator adds the specified version to the headers"""
    def decorator(f):
        def decorated_function(*args, **kwargs):
            resp = f(*args, **kwargs)
            return resp, 200, {'X-api-version': version}
        return decorated_function
    return decorator
