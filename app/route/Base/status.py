from app.DataAccess.cache import Cache
from app.route import ResourceMixin, version
from flask_restful import Resource


class StatusView(Resource, ResourceMixin):
    available_routes = ['/status']
    api_version = 'all'

    @version(api_version)
    def get(self):
        cache = Cache()
        redis_status = "error"

        if cache.connected():
            redis_status = "we have redis"
        return {"server_status": "its all gravy", "redis_status": redis_status}
