from app.lib.soap_client import SoapClient
from app.route import (
    ResourceMixin,
    version
)
from app.lib.Exceptions import (
    UnexpectedError
)

from flask_restful import (
    Resource,
    fields
)

resource_fields = {
    'data': fields.Raw
}

client = SoapClient()


class StopViewV2(Resource, ResourceMixin):
    available_routes = ['/v2/stop', '/v2/stop/<stop_id>/']
    api_version = 'v2'

    @version(api_version)
    def get(self, stop_id=None):
        if stop_id is None:
            stops = client.get_all_stops()
            if stops is None:
                raise UnexpectedError
            return stops
        stop = client.get_stop_info(stop_id)
        return stop
