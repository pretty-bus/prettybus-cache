from app.lib.soap_client import SoapClient
from app.route import ResourceMixin, version
from app.lib.Exceptions import (
    UnexpectedError
)

from flask_restful import (
    Resource,
    fields
)

resource_fields = {
    'data': fields.Raw
}

soap_client = SoapClient()


class RTPIViewV2(Resource, ResourceMixin):
    available_routes = ['/v2/rtpi/stop/<stop_id>/']
    api_version = 'v2'

    @version(api_version)
    def get(self, stop_id=None):
        if stop_id is None:
            raise UnexpectedError
        stop = soap_client.get_rtpi(stop_id)
        return stop
