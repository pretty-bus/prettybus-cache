from app.lib.db_client import DBClient
from app.route import ResourceMixin, version
from app.lib.Exceptions import (
    UnexpectedError
)

from flask_restful import (
    marshal_with,
    Resource,
    request,
    fields,
    abort,
)

resource_fields = {
    'data': fields.Raw
}

dbClient = DBClient()


class StopViewV1(Resource, ResourceMixin):
    available_routes = ['/stop', '/stop/<stop_id>/',
                        '/v1/stop', '/v1/stop/<stop_id>/']
    api_version = 'v1'

    @version(api_version)
    def get(self, stop_id=None):
        if stop_id is None:
            stops = dbClient.get_all_stops()
            if stops is None:
                raise UnexpectedError
            return stops
        stop = dbClient.get_stop_info(stop_id)
        return stop
