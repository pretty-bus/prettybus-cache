from app.lib.db_client import DBClient
from app.route import ResourceMixin, version
from app.lib.Exceptions import (
    UnexpectedError
)

from flask_restful import (
    Resource,
    fields
)

resource_fields = {
    'data': fields.Raw
}

dbClient = DBClient()


class RTPIViewV1(Resource, ResourceMixin):
    available_routes = ['/v1/rtpi/stop/<stop_id>/', '/rtpi/stop/<stop_id>/']
    api_version = 'v1'

    @version(api_version)
    def get(self, stop_id=None):
        if stop_id is None:
            raise UnexpectedError
        stop = dbClient.get_rtpi(stop_id)
        return stop
