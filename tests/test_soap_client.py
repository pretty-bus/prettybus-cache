import unittest
import requests
from nose.tools import *
from mock import MagicMock, patch

from app.lib.soap_client import SoapClient
from app.lib.Exceptions import DownStreamError


class FakeRTPIData():
    def __init__(self, tag, text):
        self.tag = tag
        self.text = text


MockClient = MagicMock()
MockClient.service = MagicMock()
MockClient.service.GetDestinations.return_value = {'Destination': [
    {
        "Description": "Abbey Rd, Abbey Park",
        "Latitude": 53.284897,
        "Longitude": -6.160525,
        "StopNumber": 2025
    },
    {
        "Description": "Abbey Rd, Abbey Avenue",
        "Latitude": 53.284897,
        "Longitude": -6.160525,
        "StopNumber": 20
    }
]}

MockClient.service.GetRealTimeStopData.return_value = {
    '_value_1': [
        'bunk',
        [
            [
                [
                    FakeRTPIData(
                        "MonitoredVehicleJourney_PublishedLineName",
                        "27"
                    ),
                    FakeRTPIData(
                        "MonitoredCall_ExpectedArrivalTime",
                        "2017-10-27T18:27:10+01:00"
                    ),
                    FakeRTPIData(
                        "MonitoredVehicleJourney_DestinationName",
                        "Clarehall"
                    ),
                    FakeRTPIData("these", "should"),
                    FakeRTPIData("be", "removed")
                ]
            ]
        ]
    ]
}

MockClient_failure = MagicMock()
MockClient_failure.service.GetRealTimeStopData.return_value = {
    '_value_1': [
        'bunk',
        []
    ]
}


class TestSoapClient(unittest.TestCase):
    @patch('app.lib.soap_client.Cache')
    @patch('app.lib.soap_client.Client', return_value=MockClient)
    def test_get_all_not_in_cache(self, mocked_client, mocked_cache):
        """does a soap request if get_all_stops not in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_all_stops.return_value = '[]'

        client = SoapClient()
        stops = client.get_all_stops()

        self.assertTrue(MockClient.service.GetDestinations.called)
        self.assertTrue(mocked_cache.get_all_stops.called)
        self.assertEqual(len(stops), 2)
        self.assertEqual(stops[0]['StopNumber'], 2025)

    @patch('app.lib.soap_client.Cache')
    @patch('app.lib.soap_client.Client', return_value=MockClient)
    def test_get_rtpi_not_in_cache(self, mocked_client, mocked_cache):
        """does a soap request if get_rtpi not in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_rtpi.return_value = '[]'

        client = SoapClient()
        rtpi = client.get_rtpi("2311")

        self.assertTrue(MockClient.service.GetRealTimeStopData.called)
        self.assertTrue(mocked_cache.get_rtpi.called)
        self.assertEqual(len(rtpi), 1)
        self.assertTrue('these' not in rtpi[0])

    @patch('app.lib.soap_client.Cache')
    @patch('app.lib.soap_client.Client', return_value=MockClient_failure)
    def test_get_rtpi_no_data_returned(self, mocked_client, mocked_cache):
        """raises an error when no useful data returned"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_rtpi.return_value = '[]'

        client = SoapClient()
        self.assertRaises(
            DownStreamError,
            client.get_rtpi,
            "2311"
        )

    @patch('app.lib.soap_client.Cache')
    @patch('app.lib.soap_client.Client', return_value=MockClient)
    def test_get_stop_info_not_in_cache(self, mocked_client, mocked_cache):
        """does a soap request if get_stop_info not in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = '[]'

        client = SoapClient()
        stop = client.get_stop_info('20')

        self.assertTrue(MockClient.service.GetDestinations.called)
        self.assertTrue(mocked_cache.get_stop_info.called)
        self.assertEqual(stop['StopNumber'], 20)
        self.assertEqual(stop['Description'], "Abbey Rd, Abbey Avenue")
