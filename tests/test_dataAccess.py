import unittest
from nose.tools import *
from test.test_support import EnvironmentVarGuard
from app.DataAccess.config import get_current_config


class TestDataAccess(unittest.TestCase):
    def test_get_current_config(self):
        """Correct configs are returned depending on environment"""
        with EnvironmentVarGuard() as environ:
            from app.config import BaseConfig
            from app.config.devConfig import DevConfig
            from app.config.testConfig import TestConfig
            from app.config.herokuConfig import HerokuConfig

            environ.clear()
            config = get_current_config()
            assert_true(isinstance(config, BaseConfig))

            environ['ENV'] = 'TEST'
            config = get_current_config()
            assert_true(isinstance(config, TestConfig))

            environ['ENV'] = 'DEV'
            config = get_current_config()
            assert_true(isinstance(config, DevConfig))

            environ['ENV'] = 'HEROKU'
            config = get_current_config()
            assert_true(isinstance(config, HerokuConfig))
