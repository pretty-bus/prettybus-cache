import unittest
import requests
from nose.tools import *
from mock import MagicMock, patch
from flask_restful import HTTPException
from requests.exceptions import Timeout

from app.lib.Exceptions import *
from app.lib.db_client import DBClient


class mockException(Exception):
    code = 500


class TestDBclient(unittest.TestCase):
    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_all_not_in_cache(self, mocked_requests, mocked_cache):
        """does a request if nothing in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_all_stops.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '{"errorcode":"0"}'
        mocked_response.json.return_value = {"errorcode": "0"}
        mocked_response.status_code = 200

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        stops = client.get_all_stops()

        self.assertTrue(mocked_cache.get_all_stops.called)
        self.assertTrue(mocked_requests.get.called)
        self.assertTrue(mocked_cache.store_all_stops.called)
        self.assertEqual(stops, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_all_in_cache(self, mocked_requests, mocked_cache):
        """does nothing if stops in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_all_stops.return_value = '{"errorcode":"0"}'

        mocked_requests.get = MagicMock()

        client = DBClient()
        stops = client.get_all_stops()

        self.assertTrue(mocked_cache.get_all_stops.called)
        self.assertFalse(mocked_requests.get.called)
        self.assertEqual(stops, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_stop_info_in_cache(self, mocked_requests, mocked_cache):
        """does nothing if stop in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = '{"errorcode":"0"}'

        mocked_requests.get = MagicMock()

        client = DBClient()
        stops = client.get_stop_info('2371')

        self.assertTrue(mocked_cache.get_stop_info.called)
        self.assertFalse(mocked_requests.get.called)
        self.assertEqual(stops, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_rtpi_in_cache(self, mocked_requests, mocked_cache):
        """gets rtpi info from cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_rtpi.return_value = '{"errorcode":"0"}'

        mocked_requests.get = MagicMock()

        client = DBClient()
        stops = client.get_rtpi('2371')

        self.assertTrue(mocked_cache.get_rtpi.called)
        self.assertFalse(mocked_requests.get.called)
        self.assertEqual(stops, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_rtpi_not_in_cache(self, mocked_requests, mocked_cache):
        """does a request if rtpi not in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_rtpi.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '{"errorcode":"0"}'
        mocked_response.json.return_value = {"errorcode": "0"}
        mocked_response.status_code = 200

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        rtpi = client.get_rtpi('2311')

        self.assertTrue(mocked_cache.get_rtpi.called)
        mocked_cache.store_rtpi.assert_called_with(
            '2311',
            '{"errorcode":"0"}'
        )
        self.assertTrue(mocked_requests.get.called)
        self.assertEqual(rtpi, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_get_stop_info_not_in_cache(self, mocked_requests, mocked_cache):
        """does a request if stop not in the cache"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '{"errorcode":"0"}'
        mocked_response.json.return_value = {"errorcode": "0"}
        mocked_response.status_code = 200

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        stops = client.get_stop_info('2311')

        self.assertTrue(mocked_cache.get_stop_info.called)
        self.assertTrue(mocked_requests.get.called)
        self.assertEqual(stops, '{"errorcode":"0"}')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_StopNotFoundError_downstream_error(
            self,
            mocked_requests,
            mocked_cache):
        """rasises stop not found form downstream"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '{"errorcode":"1"}'
        mocked_response.json.return_value = {"errorcode": "1"}
        mocked_response.status_code = 200

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        self.assertRaises(StopNotFoundError, client.get_stop_info, '2311')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_StopNotFoundError(
            self,
            mocked_requests,
            mocked_cache):
        """rasises stop not found"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '404 not found'
        mocked_response.status_code = 404

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        self.assertRaises(StopNotFoundError, client.get_stop_info, '2311')

    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_DownStreamError_bad_gateway(
            self,
            mocked_requests,
            mocked_cache):
        """rasises DownStreamError on bad gateway"""
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = None

        mocked_response = MagicMock()
        mocked_response.text = '502 not found'
        mocked_response.status_code = 502

        mocked_requests.get.return_value = mocked_response

        client = DBClient()
        self.assertRaises(DownStreamError, client.get_stop_info, '2311')

    @patch('app.lib.db_client.DownStreamError', new=mockException)
    @patch('app.lib.db_client.Cache')
    @patch('app.lib.db_client.requests')
    def test_DownStreamError_timeout(
            self,
            mocked_requests,
            mocked_cache):
        """rasises DownStreamError on timeout"""
        # TODO: why does this test require a mockException whereas the others don't
        mocked_cache = mocked_cache.return_value
        mocked_cache.get_stop_info.return_value = None

        mocked_requests.get.side_effect = Timeout()

        client = DBClient()
        self.assertRaises(mockException, client.get_stop_info, '2311')
