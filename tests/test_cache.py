import unittest
import mock
from nose.tools import *
from app.DataAccess.cache import Cache
from test.test_support import EnvironmentVarGuard


class TestCache(unittest.TestCase):
    @mock.patch('app.DataAccess.cache.redis.StrictRedis')
    def test_init_config(self, mocked_redis):
        """test that the correct redis config is used for heroku"""
        with EnvironmentVarGuard() as environ:
            environ.clear()
            environ['ENV'] = 'HEROKU'
            environ['REDIS_URL'] = 'redis://p:red_pw@localhost:3333'
            cache = Cache()

            self.assertTrue(mocked_redis.called)
            mocked_redis.assert_called_with(
                host='localhost',
                password='red_pw',
                port=3333
            )

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_stop_not_in_cache(self, mocked_redis_get):
        """returns None for stop not in cache"""
        mocked_redis_get.return_value = None
        cache = Cache()
        result = cache.get_stop_info('2311')

        self.assertIsNone(result)
        self.assertTrue(mocked_redis_get.called)

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_stop_not_in_cache(self, mocked_redis_get):
        """returns default value for if data not in cache"""
        mocked_redis_get.return_value = None
        cache = Cache()
        result = cache.get_stop_info('2311', '{}')

        self.assertIsNotNone(result)
        self.assertEqual(result, '{}')
        self.assertTrue(mocked_redis_get.called)

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_stop_in_cache(self, mocked_redis_get):
        """returns data for stop in cache"""
        mocked_redis_get.return_value = '{"some":"json"}'
        cache = Cache()
        result = cache.get_stop_info('2311')

        self.assertEqual(result, '{"some":"json"}')
        self.assertTrue(mocked_redis_get.called)

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.set')
    def test_stop_in_cache(self, mocked_redis_set):
        """set uses prefix"""
        cache = Cache(prefix="blah")
        cache.store_stop_info('2311', '{"blah":"args"}')

        mocked_redis_set.assert_called_with(
            'blah_stop_2311',
            '{"blah":"args"}',
            ex=3600
        )

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_stop_in_cache(self, mocked_redis_get):
        """get uses prefix"""
        mocked_redis_get.return_value = '{"some":"json"}'
        cache = Cache(prefix="blah")
        result = cache.get_stop_info('2311')

        self.assertEqual(result, '{"some":"json"}')
        self.assertTrue(mocked_redis_get.called)
        mocked_redis_get.assert_called_with(
            'blah_stop_2311'
        )

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.set')
    def test_store_stop_in_cache(self, mocked_redis_set):
        """stores data in the cache"""
        cache = Cache()
        cache.store_stop_info('2311', '{"some":"json"}')

        self.assertTrue(mocked_redis_set.called)
        mocked_redis_set.assert_called_with(
            'stop_2311',
            '{"some":"json"}',
            ex=3600
        )

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_get_rtpi_in_cache(self, mocked_redis_get):
        """returns data for rtpi in cache"""
        mocked_redis_get.return_value = '{"some":"json"}'
        cache = Cache()
        result = cache.get_rtpi('2311')

        self.assertEqual(result, '{"some":"json"}')
        self.assertTrue(mocked_redis_get.called)

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.set')
    def test_store_rtpi_in_cache(self, mocked_redis_set):
        """stores rtpi data in the cache"""
        cache = Cache()
        cache.store_rtpi('2311', '{"some":"json"}')

        self.assertTrue(mocked_redis_set.called)
        mocked_redis_set.assert_called_with(
            'rtpi_2311',
            '{"some":"json"}',
            ex=5
        )

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.get')
    def test_get_all_stops_in_cache(self, mocked_redis_get):
        """returns data for all stops in cache"""
        mocked_redis_get.return_value = '{"some":"json"}'
        cache = Cache()
        result = cache.get_all_stops()

        self.assertEqual(result, '{"some":"json"}')
        self.assertTrue(mocked_redis_get.called)

    @mock.patch('app.DataAccess.cache.redis.StrictRedis.set')
    def test_store_all_stops_in_cache(self, mocked_redis_set):
        """stores all stop data in the cache"""
        cache = Cache()
        cache.store_all_stops('{"some":"json"}')

        self.assertTrue(mocked_redis_set.called)
        mocked_redis_set.assert_called_with(
            'all_stops',
            '{"some":"json"}',
            ex=3600
        )
