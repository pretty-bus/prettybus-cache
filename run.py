from app.server import application

if __name__ == '__main__':

    @application.route('/')
    def index():
        return "boom"

    application.run(host='0.0.0.0', port=8000)
